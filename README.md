# Technial Program for Research Officer Position - REQ08086

The sample code contains developed programming from 4 different projects. Two projects were made while studying at the
University of Essex which are eeg-videomem-main (CE903 Group
Project), inducing-forgetting (CE902 Dissertation). The others two were developed when I was working at National
Astronomical Research Institute of
Thailand. Each project is differ by its requirements which can be
addressed here:

- eeg-videomem: Machine Learning on EEG data with heavily imbalanced labels
- inducing-forgetting: the psycho-physiological experiment design and development
- HI-FAST: data-intensive cosmological simulation statistical analysis
- HPC Operation Diagram: Linux system administrator

Please note that all the programming materials here are genuine
and copyrighted which should not be copied and distributed in any way.

## eeg-videomem-main

This project is a part of CE903-7-SP Group Project where I made a Machine Learning classification model whether the
**EEG** signals from participants watching a short video clip is memorable or not. This task was sub-task 3
in [MediaEval 2022](https://arxiv.org/abs/2212.06516) where the data is **heavily imbalanced** with 12% of positive
class. The feature extraction technique was applied to Event-Related Spectral Potentials (ERSP). Judging
from ERSP signals, the model can make classification with the average precision score of 0.931 ± 0.028. Please look at
these following files:

* `./src/Task3_preRdF_ERSP_ip22667.ipynb` contains preprocessing and feature extraction lines for ERSP data.
* `./src/Task3_RdF19_ip22667.ipynb` contains classification model and classification reports for participant 19

## inducing-forgetting

The dissertation for MSc study at University of Essex which imitating the paper on
nature, [Inducing forgetting of unwanted memories through subliminal reactivation](https://www.nature.com/articles/s41467-022-34091-1).
I developed the whole psycho-physiological experiment from scratch in professional manner. This project involves data
collection from human participants, experiment design, program porting, signal trigger, logging, testing and demo,
and legacy system adaptation. Please look at these following files:

* `./src/leaningPhase.py` is the first part of the program, the Learning Phase. The second and third parts are not
  provided here.
* `./src/main.py` is the _main programing interface_. Each phase of the experiment is called by running this file with
  the proper argument flags (run `python main.py -h` for more info)

## HI-FAST

`HI-FAST/p4_analysis.py` was written in 2020 when I was working at National Astronomical Research Institute of
Thailand (NARIT). It was the forth part of the statistical analysis where I applied Jackknife estimator for the galaxy
evolution simulation with 288 millions galaxies. The project involves processing high volume of input data and
pseudo-random generator with a length of 2exp(377).

## HPC Operation Diagram

The diagram shows the complexity of the High-performance computing system infrastructure at National Astronomical
Research Institute of Thailand (NARIT). This HPC system is similar to Ceres system at the University of Essex. I was
working as a system administrator for over three years providing offline and online supports and in-house software
development including software installation, upgrading and optimisation, resources quota management, troubleshooting and
technical supports, and system migration, benchmarking, and security monitoring. In addition, I developed the whole
monitoring system and notification system which employ combination of software: telegraf, influxDB, Grafana and
Telegram.

![alt text](./HPC Diagram.drawio.png "NARIT HPC Operation Diagram")