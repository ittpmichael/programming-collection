# Forgetting Memory

## Description

Replicate and further experiment of Inducing forgetting of unwanted memories through subliminal
reactivation ([original paper](https://www.nature.com/articles/s41467-022-34091-1))

### Project status

We are collecting data from the experiment. PID used: 100, 29

### Quick Links

[Box Storage](https://essexuniversity.app.box.com/folder/189895740978): where we keep BioSemi data and log files

[Shadow Materials](https://docs.google.com/spreadsheets/d/1lyr5RAzrLJog8-aRS-yFb_QA7UmG0cYgMsSy-1u6BCo/edit?usp=sharing): we also store results here.

Criterion for Gist ([used version](https://docs.google.com/document/d/1JjC6KruSkvdLyDSJB-ZQBLsB-axT2EvFL8N0fCbbwVs/edit?usp=sharing), [original](https://docs.google.com/document/d/1T3vzID5f6hckLKo85muMqcJASMT5W1Yvb8Eje4-TYZM/edit?usp=sharing)) and [score details](https://docs.google.com/document/d/1tiMaxyZWUlCgvC98fqOULO-rYVy87eP6izdtauqFe04/edit?usp=sharing)

## Installation

The lastest PsychoPy package is working on python3.8.17, no newer version supports it. For pip user, install
psychopy==2021.2.3, tzlocal, pandas. For conda user, simply run

```bash
conda env create -f environment.yml
```

### BioSemi

BioSemi requires gcc-5 and libusb (for usb.h). On some system (Arch Linux), it is called, libusb-compat. To compile, run

```bash
make OS=linux -B
```

## Usage

The code requires superuser (sudo/admin) privilege to run, e.g. `sudo python ./src/example.py`. Please run the code on
the project's base directory. For the lab PC, we need to activate python 3.8 using pyenv:

```shell
[user@pc ~/.../CE901_name]$ source ./script/envSetup.sh
(miniconda-3.x.x) [user@pc ~/.../CE901_name]$ conda activate py38
(py38) [user@pc ~/.../CE901_name]$

```

Next, we have to parse the enviroment variables to the superuser env (sudo), run

```shell
sudo env "PATH=$PATH VIRTUAL_ENV=$VIRTUAL_ENV PYTHONPATH=$PYTHONPATH" \
python ./src/main.py --mode learning --pid <pid> --age <age> --gender <(fe)male> \
--handedness <left|right> --demo --fullscr
```

The experiment mode needs to be run in-order, by selecting the flag: `--mode` from `learning`, `tnt` and `testing`.

## Roadmap

### Learning Phase

* [X]  Experiment on terminal
* [X]  Experiment on-screen
* [X]  show a word
* [X]  show an image
* [X]  Cue-pair position (side by side or ?) side by side, yes
* [X]  Participant info
* [X]  Age, Gender, handedness
* [X]  Log random state number
* [X]  ID, date
* [X]  time taking the experiment for each phase
* [X]  Adding random state for each person
* [X]  create a random state
* [X]  apply argparse

### Test Feedback

* [X]  Experiment on terminal
* [X]  Experiment on-screen
* [X]  show a word
* [X]  show an image
* [X]  Logging

### TNT phase

* [ ]  ~~Experiment on terminal~~
* [X]  apply argparse
* [X]  get pid as random state
* [X]  Experiment on-screen

### Logging

### Integrate with devices

* [X]  EEG
* [ ]  EDA

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.
