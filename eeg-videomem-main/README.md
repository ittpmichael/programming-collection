# Prediction of Video Memorability and EEG Signals

*Experiment with machine learning models for prediction of video memorability and EEG signals using data from
MediaEval2022*

## Important notices

- **DEADLINE** 23rd March 2023
- Expected coding deadline 18th March 2023

## Directory

- [Minute summary](https://docs.google.com/document/d/1GuXejxxL9rMFbr5cCyToUCU3zeyNTb8IflIFLpav0yM)
- [Our Final Report](https://www.overleaf.com/project/63eb966c514ff4a4fd7236a2)
- [Rhaju Colab](https://colab.research.google.com/drive/1NjL5o4JwAh4SlLWmhJQDbGMk7mIT-zEa)
- Main task (3), please
  follow [Understanding Media Memorability From Event-Related Potential Records And Visual Semantics:](https://2022.multimediaeval.com/paper6167.pdf)
  *Ricardo Kleinlein, Enrique Rodríguez Sebastián, Fernando Fernández-Martínez*,
  [Overview of the EEG Pilot Subtask at MediaEval 2021: Predicting Media Memorability](https://arxiv.org/abs/2201.00620)
  *Sweeney, L., Matran-Fernandez, A., Halder, S., de Herrera, A.G.S., Smeaton, A. and Healy,
  G., 2021.*
- Starter ML code -> https://osf.io/zt6n9/


- Competition link https://2022.multimediaeval.com/
- [CSEE Jira](https://cseejira.essex.ac.uk/)
- [Project Readme and data guideline](https://github.com/multimediaeval/2022-Predicting-Media-Memorability-Task/),
  Data_guide.md (backup)

## Program Directory

- **environment.yml** contains list of library needed for setting up new conda environment. Please use command
`conda env create -f environment.yml`. If you use pip please make sure these following libraries are installed:

  - pandas, numpy, scipy
  - scikit-learn
  - seaborn, matplotlib
  - tensorflow
- **data** should contain files used for coding. Please download the data from the link below. *Do not* add files to the
repository, becasue the data is too big.
- **docs_guide** helpful guides are put here.
- **docs_paper** find all papers here
- **img** exported images and plots
- **src** main source files, e.g. .ipynb, .py, .sh
- **src_previous** source files from the previous researches and projects
- **README.md** this readme

## Accessing Data

To download the data visit
the [link](https://drive.google.com/drive/folders/14Q_R8PALTuMECR-lBWq1LcC3KGFjeVC-?usp=sharing).

There you will find three folders, "Memento10k Data", "VideoMem Data" and "EEG Data" associated with the two datasets,
and with the EEG task each of them containing:

- **training_set:** the initial training set associated with this task
- **development_set:** additional set that can be used both as additional training data and/or for testing your methods
  on a separate set of videos
- **testing_set:** the testing set that participants will run their methods on and submit their results in order to see
  method performance

You will also have to download the Memento10k videos separately in an archive, from
this [link](https://www.dropbox.com/s/9s66yyi22ymcwtw/memento_release_v1.zip?dl=0).

**_It is important that you use the provided link / archive download for Memento video download, as some urls for
the videos may no longer be active, and the archive contains them all. We will provide the VideoMem videos inside the
folder associated with the 2022 Predicting Media Memorability task._**

### Data description

#### ERSP Data Description

The file `ERSP_data.csv` contains EEG power (event-related spectral potentials) at different sensors distributed across
the scalp of the participants over time and different frequency bands. Each row represents one trial.

The format of the column names is Sensor_frequency_time, where:

* there are 28 sensors
* frequency is the frequency (in Hz) between 2--29.
* time represents the time within the trial. time=0 corresponds to 1 second before the start of the video, and time=4
  corresponds to the end of the video (3 seconds).

* The last 3 columns are:

subject -- participant ID
filename -- name of the video that corresponds to the trial
LABEL -- '1' if the video was remembered by the participant; '2' if the video was not remembered. Missing values
represent the test set.

#### ERP Data Description

The file `ERP_data.csv` contains EEG amplitudes at different sensors distributed across the scalp of the participants
and
over time. Each row represents one trial.

The format of the column names is Sensor_time, where:

- time=0 corresponds to the start of the video and time=29 corresponds to 1 second after the start of the video.
- there are 28 sensors.

The last 3 columns are:

- subject -- participant ID
- filename -- name of the video that corresponds to the trial
- LABEL -- '1' if the video was remembered by the participant; '2' if the video was not remembered. Missing values
  represent the test set.

### Submission instructions

**Subtask 3**

The following instructions must be followed when uploading your runs:

1. You will receive a link to a Google Drive location where you can upload your runs.
2. 5 runs are allowed for each team for the EEG task.
3. The following naming convention must be respected. Each file must have the following naming template:
    - eeg22mem_teamname_runname.csv, where
    - eeg22mem is a mandatory indicator (keep it like this)
    - teamname represents your team's name as you chose it during registration (e.g., AIMultimediaLab)
    - runname represents the name you decide to give this run (e.g.: run1, runCNNResNet, etc)
4. So a correct runname would be: eeg22mem_AIMultimediaLab_runCNN.csv
5. As we mentioned, the submissions must be sent by the 21st November, 23:59 Anywhere-on-Earth time.

The file for the EEG task must respect the following format: first column represents the participant_id, second column
represents the video_filename and third column represents the prediction.

```text
Participant_id, video_filename, mem_score
2,hiking_flickr-8-1-8-5-8-7-6-6-2581858766_18.mp4,0.3318
2,laughing_flickr-5-5-1-2-1-8-6-4-5655121864_49.mp4,0.7467
10,constructing_flickr-7-1-2-1-9-6-0-3-24471219603_88.mp4,0.1224
```

**Subtask 1 and Subtask 2**

The following instructions must be taken into account when uploading your runs:

1. You will receive a link to a Google Drive location where you can upload your runs.
2. 5 runs are allowed for each team for each of the 3 subtasks.
3. The following naming convention must be respected. Each file must have the following naming template:
    - me22mem_teamname_subtask_runname.csv, where
    - me22mem is a mandatory indicator (keep it like this)
    - teamname represents your team's name as you chose it during registration (e.g.: AIMultimediaLab, etc)
    - subtask represents the subtask the run is for (one of these three values: subtask1, subtask2, subtask3 for one of
      the three subtasks as described in the SubTasks section)
    - runname represents the name you decide to give this run (e.g.: run1, runCNNResNet, etc)
4. So a correct runname would be: me22mem_AIMultimediaLab_subtask2_runCNN.csv
5. As we mentioned, the submissions must be sent by the 21st November, 23:59 Anywhere-on-Earth time.

For the first two subtasks (Subtask 1 : How memorable is this video? - Video-based prediction and ~~Subtask 2 : How
memorable is this video?- Generalization)~~ the file must respect the following format: first column represents the
video_id, and second column represents the memorability score.

```text
video_id,mem_score
1,0.3318
3,0.7467
6,0.1224
```
