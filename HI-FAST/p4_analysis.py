import argparse
import logging
import os

import numpy as np
import pandas as pd

import utils

LOGGER = logging.getLogger(__name__)


def calc_ls(Nrand, Nmock, DD, DR, RR):
    DR /= 2.
    return 1. + (Nrand / Nmock) ** 2 * DD / RR - 2 * (Nrand / Nmock) * DR / RR


def calc_hm(DD, DR, RR):
    # DR = DR/2.
    return (DD * RR / DR / DR) - 1.


# combine_wedges_jk combines data from FtF data into an artificial jackknife data
def combine_wedges_jk(data_, summary, **kwargs):
    for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
        bin_name = 'b' + str(mass_bin)
        bin_name_comb = bin_name + 'comb'
        # create zero values pd.DataFrame
        data_[bin_name_comb] = pd.DataFrame(data=0., columns=data_[next(iter(data_))].columns,
                                            index=data_[next(iter(data_))].index)
        # copy 'x' (as comoving dist.) from an existing 'x'
        data_[bin_name_comb]['x'] = data_[next(iter(data_))]['x']
        # sum to get combined bin results
        for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
            for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                name_suffix = utils.get_name_suffix(mass_bin, ra_bin, dec_bin)
                data_[bin_name_comb][['DD', 'DR', 'RR']] += data_[name_suffix][['DD', 'DR', 'RR']]
        # artificial JK
        col_rand = 'parNumRandJK'
        col_mock = 'parNumMockJK'
        for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
            for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                name_suffix = utils.get_name_suffix(mass_bin, ra_bin, dec_bin)
                name_suffix_jk = name_suffix + 'jk'
                data_[name_suffix_jk] = pd.DataFrame(columns=data_[next(iter(data_))].columns,
                                                     index=data_[next(iter(data_))].index)
                data_[name_suffix_jk]['x'] = data_[next(iter(data_))]['x']
                data_[name_suffix_jk][['DD', 'DR', 'RR']] = data_[bin_name_comb][['DD', 'DR', 'RR']] - \
                                                            data_[name_suffix][['DD', 'DR', 'RR']]

                Nrand = utils.select_summary(summary, 0, ra_bin, dec_bin, col_rand)
                Nmock = utils.select_summary(summary, mass_bin, ra_bin, dec_bin, col_mock)
                data_[name_suffix_jk]['xi'] = calc_ls(Nrand, Nmock, data_[name_suffix_jk]['DD'],
                                                      data_[name_suffix_jk]['DR'] * 2,
                                                      data_[name_suffix_jk]['RR'])
                data_[name_suffix_jk]['xi_hm'] = calc_hm(data_[name_suffix_jk]['DD'],
                                                         data_[name_suffix_jk]['DR'],
                                                         data_[name_suffix_jk]['RR'])

    return data_


class ReadOutput():
    def __init__(self, **kwargs):
        """
        Init output reading class
        :param kwargs:
        """
        self.data_path = kwargs['data_path']
        self.corr_type = ['xi_ls', 'xi_hm']
        # TODO check these numbers again
        self.nrand_all = 288000000  # data with redsift >= 0.16 is removed.
        self.nmock_all_ = {'b0': 23266926, 'b1': 23266926, 'b2': 23266926, 'b3': 23266929}
        self.err_type = kwargs['err_type']
        self.raw_data = self.data_re_bin = None

        # define new bins here
        self.rrange = kwargs['rmax'] - kwargs['rmin']
        self.step_size = self.rrange / kwargs['num_step']
        self.new_bins = np.arange(kwargs['rmin'], kwargs['rmax'] + self.step_size, self.step_size)

    def read_data_wedges(self, output_path, name_prefix, **kwargs):
        """
        Reading data from each wedge or each JK chunk into a big dict object. If you're reading only a upper/lower half
        (a hemisphere), do set the dec_bin to [0,1] or [2,3]. The data reading from CUTE output has 6 columns, which
        are 'x', 'xi', 'DD', 'DR' and 'RR', respectively.
        :param output_path: output_path means an output from CUTE, but it's actually an input_path.
        :param name_prefix: a prefix name to find a certain data to read, e.g. 'corr_gpu_JK_b0r0d0.dat'
        :param kwargs:
        :return: self
        """
        LOGGER.info("Looking for files to read in {}...".format(self.data_path))
        self.raw_data = {}
        dtype_ = {'DD': np.float64, 'DR': np.float64, 'RR': np.float64}
        for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
            # Read 'b[0-3]'
            bin_name = 'b' + str(mass_bin)
            file_name = os.path.join(output_path, 'corr_gpu_' + bin_name + '.dat')
            self.raw_data[bin_name] = pd.read_csv(file_name, index_col=None, names=['x', 'xi', 'DD', 'DR', 'RR'],
                                                  header=None, sep=' ', dtype=dtype_)
            LOGGER.info("Read {} from {}".format(bin_name, file_name))
            # Iterate through 'JK' and 'FtF'
            for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
                for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                    name_suffix, name_suffix_full = utils.get_name_suffix(mass_bin, ra_bin, dec_bin, self.err_type)
                    file_name = os.path.join(output_path, name_prefix + name_suffix + '.dat')
                    self.raw_data[name_suffix_full] = pd.read_csv(file_name, index_col=None,
                                                                  names=['x', 'xi', 'DD', 'DR', 'RR'], header=None,
                                                                  sep=' ', dtype=dtype_)
                    LOGGER.info("Read {} from {}".format(name_suffix_full, file_name))

        LOGGER.info("File reading process is completed and saved to [self.raw_data].")
        return self

    def re_bins(self, summary, **kwargs):
        """
        Re-binning the data by creating a new larger bin size and sum all the data in that bin range together. After
        that calculate Xi Landy-Szalay and Hamilton estimations.
        :param summary: The summary file contains information of data of each wedges such as HI mass ranges, amount of
                        particle for each wedge or each JK chunk.
        :param kwargs:
        :return: self
        """
        LOGGER.info("Starting to re-bin the data...")
        col_rand = col_mock = None
        LOGGER.info("The data has been re-bin into {} bins.".format(kwargs['num_step']))

        self.data_re_bin = {}
        x_range = self.raw_data[next(iter(self.raw_data))]['x']

        if self.err_type == 'JK':
            col_rand = 'parNumRandJK'
            col_mock = 'parNumMockJK'
        elif self.err_type == 'FtF':
            col_rand = 'parNumRand'
            col_mock = 'parNumMock'

        for item in iter(self.raw_data):
            # Re-binning the whole sample set here
            if item in ['b0', 'b1', 'b2', 'b3']:
                self._re_bins(item, x_range, self.nmock_all_[item], self.nrand_all)
                LOGGER.info("Re-binning {}...".format(item))
            else:
                Nmock = utils.select_summary(summary, item[1], item[3], item[5], col_mock)
                Nrand = utils.select_summary(summary, 0, item[3], item[5], col_rand)
                # Real re-binning is here
                self._re_bins(item, x_range, Nmock, Nrand)
                LOGGER.info("Re-binning {}...".format(item))
        return self

    def _re_bins(self, col_name, x_range, nmock, nrand):
        """
        :param summary: The summary file contains information of data of each wedges such as HI mass ranges, amount of
                        particle for each wedge or each JK chunk.
        :param x_range: Comoving distance column of the raw data
        :return: self
        """
        # Drop column 'x' and 'xi' and aggregate sum other columns, DD DR RR.
        data_bins = self.raw_data[col_name].drop(['x', 'xi'], axis=1).groupby(pd.cut(x_range, self.new_bins))
        self.data_re_bin[col_name] = data_bins.agg('sum')
        self.data_re_bin[col_name] = self.data_re_bin[col_name].reset_index()
        self.data_re_bin[col_name]['x'] = self.new_bins[:-1] + self.step_size / 2.
        # re-calculate LS estimation
        self.data_re_bin[col_name]['xi_ls'] = calc_ls(nrand, nmock, self.data_re_bin[col_name]['DD'],
                                                      self.data_re_bin[col_name]['DR'],
                                                      self.data_re_bin[col_name]['RR'])
        self.data_re_bin[col_name]['xi_hm'] = calc_hm(self.data_re_bin[col_name]['DD'],
                                                      self.data_re_bin[col_name]['DR'],
                                                      self.data_re_bin[col_name]['RR'])
        return self

    # TODO try median
    @staticmethod
    def cal_average(data, xi_col_name, err, **kwargs):
        """
        :param data: input data, e.g. data_re_bin
        :param xi_col_name: xi_ls, xi_hm
        :param err: choose between JK and FtF
        :param kwargs: **kwargs
        :return: old data + b0_avgJK
        """
        num_sample = np.float(kwargs['max_ra_bin'] * kwargs['max_dec_bin'])
        for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
            # b0_avgJK
            bin_name_avg_err = 'b' + str(mass_bin) + '_' + 'avg' + str(err)
            # Check if the DataFrame exists
            try:
                data[bin_name_avg_err]
            except KeyError:
                # If not exists, create zero-filled DataFrame with one xi column
                data[bin_name_avg_err] = pd.DataFrame(0., index=data[next(iter(data))].index,
                                                      columns=['x', xi_col_name])
                data[bin_name_avg_err]['x'] = data[next(iter(data))]['x']

            # If the DF exists, check if the column exists
            # If the column exists and is not a zero-filled one, goes next.
            if (xi_col_name in data[bin_name_avg_err].columns) and any(data[bin_name_avg_err][xi_col_name] != 0.):
                pass
            # If the column doesn't exist create zero-filled one.
            else:
                # set data['b0_avgJK'] = 0.
                data[bin_name_avg_err][xi_col_name] = 0.

            for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
                for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                    # get full name: b0r0d0_JK
                    _, name_suffix_full = utils.get_name_suffix(mass_bin, ra_bin, dec_bin, err)
                    # data['b0_avgJK']['xi_ls'] += data['b0r0d0_JK']['xi_ls']
                    data[bin_name_avg_err][xi_col_name] += data[name_suffix_full][xi_col_name]
            # divide by number of sample to get an average
            # data['b0_avgJK']['xi_ls'] = data['b0_avgJK']['xi_ls']/32.
            data[bin_name_avg_err][xi_col_name] /= num_sample
        return data

    def cal_err(self, data, xi_col_name, **kwargs):
        # Now, only use xi_col_name = 'xi_ls'
        col_err = 'sum_err_' + self.err_type + '_' + xi_col_name
        for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
            bin_name = 'b' + str(mass_bin)
            bin_name_avg = bin_name + '_avg' + self.err_type
            data[bin_name_avg][col_err] = 0.
            for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
                for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                    _, name_suffix_full = utils.get_name_suffix(mass_bin, ra_bin, dec_bin, self.err_type)
                    # Calculate error by compare the results of each JK sample with the average JK sample.
                    # This code also works for 'FtF' sample, but the ratios are around 1/32 while 'JK' has the ratio
                    # around 31/32.
                    # data['b0_avgJK']['xi_ls']
                    data[bin_name_avg][col_err] += self._calc_err(DR_i=31.,
                                                                  DR=32.,
                                                                  xi_i=data[name_suffix_full][xi_col_name],
                                                                  xi=data[bin_name_avg][xi_col_name])
        return data

    @staticmethod
    def _calc_err(DR_i, DR, xi_i, xi):
        return DR_i / DR * (xi_i - xi) ** 2

    def calc_cov(self, data, xi_col_name, **kwargs):
        # 'xi_ls_JK' contains all JK subfield in one pack (DataFrame) and create zero-size dictionary.
        # We'll add members, b0-3, below.
        xi_col_name_full = xi_col_name + '_' + self.err_type
        data[xi_col_name_full] = {}
        LOGGER.info("Create zero-size dictionary containing xi every sub-field")

        # 'cxi_ls_JK'
        cxi_col_name = 'c' + xi_col_name_full
        data[cxi_col_name] = {}
        LOGGER.info("Create zero-size dictionary containing cxi every mass bin")

        for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
            bin_name = 'b' + str(mass_bin)
            bin_name_avg = bin_name + '_avg' + self.err_type
            len_bin = len(data[next(iter(data))])

            # generate zero-filled arrays
            data[xi_col_name_full][bin_name] = np.zeros((kwargs['max_ra_bin'] * kwargs['max_dec_bin'], len_bin))
            data[cxi_col_name][bin_name] = pd.DataFrame(0., index=data[next(iter(data))]['x'],
                                                        columns=data[next(iter(data))]['x'])
            k = 0
            # iterate through 32 times of measurement
            for ra_bin in range(kwargs['min_ra_bin'], kwargs['max_ra_bin']):
                for dec_bin in range(kwargs['min_dec_bin'], kwargs['max_dec_bin']):
                    # 'b0r0d0_JK'
                    _, name_suffix_full = utils.get_name_suffix(mass_bin, ra_bin, dec_bin, self.err_type)
                    data[xi_col_name_full][bin_name][k] = data[name_suffix_full][xi_col_name].values

                    # error = data['b0r0d0_JK']['xi_ls'] - data['b0_avgJK']['xi_ls']
                    xi_diff_k = data[name_suffix_full][xi_col_name].values - data[bin_name_avg][xi_col_name].values

                    # reshape the matrix, so that it can be dotted
                    xi_diff_k_reshaped = xi_diff_k.reshape(len_bin, 1)
                    # TODO check this following factor
                    # C_{ij} = \frac(N_s - N_d)/(N_d*N_JK)*\Sigma_k^{N_{JK}}{(y_i^k-\bar{y_i})*(y_j^k-\bar{y_j})}
                    data[cxi_col_name][bin_name] += np.dot(xi_diff_k_reshaped, xi_diff_k_reshaped.transpose())
                    k = k + 1
            LOGGER.info("Adding xi and cxi, mass bin {}...".format(mass_bin))

        len_bin = len(data[cxi_col_name][next(iter(data[cxi_col_name]))])

        rxi_col_name = 'r' + xi_col_name_full
        data[rxi_col_name] = {}
        LOGGER.info("Create zero-size dictionary containing rxi every mass bin")

        for mass_bin in range(kwargs['min_mass_bin'], kwargs['max_mass_bin']):
            bin_name = 'b' + str(mass_bin)
            data[rxi_col_name][bin_name] = pd.DataFrame(0., index=data[next(iter(data))]['x'],
                                                        columns=data[next(iter(data))]['x'])
            for i in range(0, len_bin):
                for j in range(0, len_bin):
                    data[rxi_col_name][bin_name].iloc[i, j] = data[cxi_col_name][bin_name].iloc[i, j] / np.sqrt(
                        data[cxi_col_name][bin_name].iloc[i, i] * data[cxi_col_name][bin_name].iloc[j, j])
            LOGGER.info("Adding rxi mass bin {}...".format(mass_bin))
        return self


def main():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s:%(process)d:%(levelname)s:%(name)s:%(message)s')
    parser = argparse.ArgumentParser(description='A script for analysing data that obtain from pair counting processes')
    parser.add_argument('--test', action='store_false', default=True)
    parser.add_argument('--data-path', default='../data-cute', help='data path (default: ../data-cute)')
    parser.add_argument('--min-mass-bin', type=int, default=0)
    parser.add_argument('--max-mass-bin', type=int, default=4, help='max number of mass bin (default: 4)')
    parser.add_argument('--min-ra-bin', type=int, default=0)
    parser.add_argument('--max-ra-bin', type=int, default=8, help='max number of ra bin (default: 8)')
    parser.add_argument('--min-dec-bin', type=int, default=0)
    parser.add_argument('--max-dec-bin', type=int, default=4, help='max number of dec bin (default: 4)')
    parser.add_argument('--rmin', type=float, default=0., help='r_min in Mpc/h (default: 0.)')
    parser.add_argument('--rmax', type=float, default=300., help='r_max in Mpc/h (default: 300.)')
    parser.add_argument('--err-type', type=str, default='JK', help='select error calculation type, only two are '
                                                                   'supported: JK and FtF (default: JK)')
    parser.add_argument('--num-step', type=float, default=64., help='number of step from r_min to r_max (default: 16)')
    kwargs = vars(parser.parse_args())
    LOGGER.debug(kwargs)
    # export env to read data from /lustre
    os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'
    # define lv2 path and read the summary file
    path_lv2 = 'b4r8d4'
    summary_path = os.path.join(kwargs['data_path'], path_lv2, 'summary.dat')
    summary = utils.read_summary(summary_path)

    path_lv2 = 'b4r8d4_output_old'
    output_path = os.path.join(kwargs['data_path'], path_lv2)

    # Initialize output reading object
    cute_out = ReadOutput(**kwargs)
    # Choose which files to read
    cute_out.read_data_wedges(output_path, 'corr_gpu_JK_', **kwargs)
    # Re-binning the data
    cute_out.re_bins(summary, **kwargs)
    LOGGER.info("Data re-binning is completed.")

    cute_out.data_re_bin = cute_out.cal_average(cute_out.data_re_bin, 'xi_ls', 'JK', **kwargs)
    # You can also parse raw data without re-binning
    cute_out.data_re_bin = cute_out.cal_err(cute_out.data_re_bin, 'xi_ls', **kwargs)
    cute_out.calc_cov(cute_out.data_re_bin, 'xi_ls', **kwargs)

    path_lv2 = 'b4r8d4_2pcf'
    output_path = os.path.join(kwargs['data_path'], path_lv2)
    xi_col_name_full = 'xi_ls_' + kwargs['err_type']

    # save to files
    '''
    for i in iter(cute_out.data_re_bin['rxi_ls_JK']):
        bin_name = str(i)  # b0
        bin_name_avg = bin_name + '_avg' + kwargs['err_type']  # b0_avgJK
        # TODO reduce these exporting code
        # export whole samples: self.data_re_bin['b0'] -> 'b0.feather', 'b0.dat'
        utils.save_both(cute_out.data_re_bin[bin_name], os.path.join(output_path, bin_name))
        # export avg from JK: self.data_re_bin['b0_avgJK'] -> 'b0_avgJK.feather', 'b0_avgJK.dat'
        utils.save_both(cute_out.data_re_bin[bin_name_avg], os.path.join(output_path, bin_name_avg))

        # Reverse row order; note that for DF use .iloc[::-1]
        # DataFrame self.data_re_bin['rxi_ls_JK']
        rxi_col_name = 'r' + xi_col_name_full
        cute_out.data_re_bin[rxi_col_name][bin_name] = cute_out.data_re_bin[rxi_col_name][bin_name].iloc[::-1]
        # export rxi: 'rxi_ls_JK_b0.feather'
        utils.save_both(cute_out.data_re_bin[rxi_col_name][bin_name],
                        os.path.join(output_path, rxi_col_name + '_' + bin_name))

        # export 32 xi: 'xi_ls_JK_b0.feather'
        cute_out.data_re_bin[xi_col_name_full][bin_name + '_df'] = pd.DataFrame(
            cute_out.data_re_bin[xi_col_name_full][bin_name],
            columns=cute_out.data_re_bin[rxi_col_name][bin_name].columns).T
        # TODO fix export to feather
        utils.save_both(cute_out.data_re_bin[xi_col_name_full][str(bin_name) + '_df'],
                        os.path.join(output_path, xi_col_name_full + '_' + bin_name))
        # utils.save_to_ascii(cute_out.data_re_bin[xi_col_name_full][str(bin_name) + '_df'],
        #                    os.path.join(output_path, xi_col_name_full + '_' + str(bin_name) + '.dat'))
    '''
    return cute_out, summary


if __name__ == '__main__':
    cute_out, summary = main()
    # data, summary = main()
